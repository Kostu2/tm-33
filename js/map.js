ymaps.ready(init);
function init(){
    let addresses = $('[data-address]');

    let myMap = new ymaps.Map("map", {
        center: [55.76, 37.64],
        zoom: 15
    });

    addresses.each(function (i) {
        ymaps.geocode(this.innerText, {results: 1}).then(function (res) {
            var firstGeoObject = res.geoObjects.get(0);
            myMap.setCenter(firstGeoObject.geometry.getCoordinates());
            var myPlacemark = new ymaps.Placemark(firstGeoObject.geometry.getCoordinates(), {
                balloonContent: '',
                hintContent: ''
            });
            myMap.geoObjects.add(myPlacemark);
        });
    });
}