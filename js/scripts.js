//// Прилипание меню
//$(window).scroll(function () {
//    if ($(this).scrollTop() > 76) {
//        $('header').addClass('sticky-top');
//    } 
//    else {
//        $('header').removeClass('sticky-top');
//    }
//});


// Кнопка наверх
$(document).ready(function () {
    $(document.body).append('<a id="back_top" href="#"><i class="fas fa-chevron-up"></i></a>');
    $('#back_top').hide();

    $(window).scroll(function () {
        if ($(this).scrollTop() > 120) {
            $('#back_top').fadeIn('slow');
        } else {
            $('#back_top').fadeOut('slow');
        }
        ;
    });

    $('#back_top').on('click', function (e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: 0
        }, 800);
        $('#back_top').fadeOut('slow').stop();
    });
});

// Слайдер Owl-Carousel

$('.product-slider').owlCarousel({
    navSpeed: 1500,
    dragEndSpeed: 1500,
    nav: false,
    dots: true,
    // loop: true,
    loop: false,
    rewind: true,
    margin: 30,
    responsive: {
        0:{
            items:1
        },
        768:{
            items:2
        },
        992:{
            items:3
        },
        1200:{
            items:4
        }
    }
});

$('.page-gallery').owlCarousel({
    navSpeed: 1500,
    dragEndSpeed: 1500,
    nav: true,
    dots: false,
    // loop: true,
    loop: false,
    rewind: true,
    margin: 30,
    responsive: {
        0:{
            items:1
        },
        768:{
            items:2
        },
        992:{
            items:3
        },
        1200:{
            items:4
        }
    }
});

$('.img-gallery').owlCarousel({
    navSpeed: 1500,
    dragEndSpeed: 1500,
    nav: true,
    dots: false,
    margin: 20,
    responsive: {
        0:{
            items:1
        },
        768:{
            items:2
        },
        992:{
            items:3
        },
        1200:{
            items:4
        }
    }
});

let formSubmitted = false;
let reviewSent = false;

$(document).on('submit', '#review-form', null, function (e) {
    if (reviewSent) {
        return false;
    }
    e.preventDefault();

    // let file = $('#request-file').prop('files')[0];
    let data = new FormData(document.querySelector('#review-form'));

    $.ajax({
        type: "POST",
        url: '/reviews/contact-form',
        data: data,
        processData: false,
        contentType: false,
        dataType: "json",
        success: function (response) {
            if (response.result == 'success') {
                $('#contact-submit').prop('disabled');
                $('#review-form input, textarea').val('');
                $('#review-form').hide();
                $('#sent-success').removeClass('d-none');
                reviewSent = true;
            }
        }
    });
    return false;
});


$(document).on('submit', '#contact-form', null, function (e) {
    if (formSubmitted) {
        return false;
    }
    e.preventDefault();
    // let file = $('#request-file').prop('files')[0];
    let data = new FormData(document.querySelector('#contact-form'));

    $.ajax({
        type: "POST",
        url: '/custom-products/contact-form',
        data: data,
        processData: false,
        contentType: false,
        dataType: "json",
        success: function (response) {
            if (response.result == 'success') {
                $('#contact-submit').text('Заявка успешно отправлена!').prop('disabled');

                $('#contact-form input, textarea').val('');
                formSubmitted = true;
            }
        }
    });
    return false;
});

let recallSubmited = false;


$(document).on('beforeSubmit', '#order-form', null, function (e) {
    if (!$('#order-agree').prop('checked')) {
        $('#order-agree-error').removeClass('d-none');
        return false;
    } else {
        $('#order-agree-error').addClass('d-none');
        e.preventDefault();
    }
});

$(document).on('submit', '#recall-form', null, function (e) {
    if (recallSubmited) {
        return false;
    }
    e.preventDefault();

    if (!$('#recall-agree').prop('checked')) {
        $('#agree-error').removeClass('d-none');
        return false;
        2
    } else {
        $('#agree-error').addClass('d-none');
    }


    // let file = $('#request-file').prop('files')[0];
    let data = new FormData(document.querySelector('#recall-form'));

    $.ajax({
        type: "POST",
        url: '/site/contact-form',
        data: data,
        processData: false,
        contentType: false,
        dataType: "json",
        success: function (response) {
            if (response.result == 'success') {
                $('#recall-submit').text('Заявка успешно отправлена!').prop('disabled');
                $('#recall-form input, textarea').val('');
                recallSubmited = true;
            }
        }
    });
    return false;
});

$(document).on('click', '[data-like],[data-dislike]', null, function (e) {
    let productId = this.dataset.like;
    let $that = $(this);
    $.ajax({
        type: "POST",
        url: '/products/to-favorites',
        data: {productId},

        success: function (response) {
            if (response == 'removed') {
                $that.find('i').removeClass('fa-heart-broken');
                $that.find('i').addClass('fa-heart');
            }
            if (response == 'added') {
                $that.find('i').removeClass('fa-heart');
                $that.find('i').addClass('fa-heart-broken');
            }
        }
    });
});

$(document).on('click', '[data-compare]', null, function (e) {
    let productId = this.dataset.compare;
    let $that = $(this);
    $.ajax({
        type: "POST",
        url: '/products/to-compare',
        data: {productId},

        success: function (response) {
            if (response == 'removed') {
                $that.find('i').removeClass('fa-window-close');
                $that.find('i').addClass('fa-retweet');
            }
            if (response == 'added') {
                $that.find('i').removeClass('fa-retweet');
                $that.find('i').addClass('fa-window-close');
            }
        }
    });
});

$(document).on('click', '[data-compareremove]', null, function (e) {
    let productId = this.dataset.compareremove;
    let $that = $(this);
    $.ajax({
        type: "POST",
        url: '/products/to-compare',
        data: {productId},

        success: function (response) {
            if (response == 'removed') {
               window.location = '/compare'
            }
        }
    });
});


// $(document).on("renderCart", function (event, data) {
//     let link = '/cart/element/update';
//
//     $.post(
//         link,
//         data,
//         function (result) {
//             console.log(result);
            // $('.products.pistol88-cart-list').replaceWith(result.view);
            // if (result.element) {
            //     $('.cart-list[data-id="' + result.element.id + '"]').find('.price-total').replaceWith(result.element.price);
            // }
            // $('.total-cart-price').replaceWith(result.totalCost);
            // $('.base-cart-price').replaceWith(result.baseCost);
            //
            // count = result.count.toString();
            // count = count.substr(count.length - 1, 1);
            //
            // if (count == 1) {
            //     count = result.count + ' Товар';
            // } else if (count > 1 && count < 5) {
            //     count = result.count + ' Товара';
            // } else {
            //     count = result.count + ' Товаров';
            // }
            //
            // $('.cart-element-count').text(result.count);
            //
            // $('.cart-footer').replaceWith(result.cart);
            //
            // if (!result.count) {
            //     $('.no_products').removeClass('unvisible');
            //     $('.cart-product').addClass('unvisible');
            // } else {
            //
            // }

        // }, "json");
// });

$(document).on('click', '[data-compare-reset]', null, function (e) {
    $.ajax({
        type: "POST",
        url: '/products/compare-reset',
        success: function (response) {
           $('.table-responsive').html('');
        }
    });
});


$(document).on('click', '[data-favremove]', null, function (e) {
    let productId = this.dataset.favremove;
    console.log(productId);
    let $that = $(this);
    $.ajax({
        type: "POST",
        url: '/products/to-favorites',
        data: {productId},

        success: function (response) {
            return $that.parents('[data-prod]').remove();
        }
    });
});



$(document).on('beforeSubmit', '#signup-form', null, function (e) {
    e.preventDefault();

    if (!$('#signup-agree').prop('checked')) {
        $('#signup-agree-error').removeClass('d-none');
        return false;
    } else {
        $('#signup-agree-error').addClass('d-none');
    }
    return true;
});

$(document).on('click', '[data-minus-button]', null, function (e) {
   let $input = $(this).parent().siblings('input');
   let value = $input.val();
   if (value > 1) {
       $input.val(--value);
   }
});

$(document).on('click', '[data-plus-button]', null, function (e) {
    let $input = $(this).parent().siblings('input');
    let value = $input.val();
    if (value < $input.prop('max')) {
        $input.val(++value);
    }
});

$('.home-slider').owlCarousel({
    navSpeed: 1500,
    dragEndSpeed: 1500,
    nav: true,
    dots: false,
    margin: 5,
    items: 1
});

$('.js-itab').on('click', function () {
    var btn = $(this);
    setTimeout(function () {
        btn.removeClass('active');
    }, 200);
});

$('#product-sort').change(function () {
    var $this = $(this);
    var value = $this.find('option:selected').attr('name');
    sortChange('sort', value);
});

function sortChange(paramName, value) {
    var url = window.location.origin + window.location.pathname + '?';
    var search = window.location.search;
    var params = new URLSearchParams(search);

    if (value > 0 || value != "") {
        params.set(paramName, value);
    } else {
        params.delete(paramName);
    }
    window.location.href = url + params.toString();
}

$('.parent > a i').on('click', function(e){
    e.preventDefault();
    $(this).closest('.parent').children('ul').toggleClass('open');
});

$('.search-toggle').on('click', function(){
    $('.search').addClass('open');
});

$('.hide-search').on('click', function(){
    $('.search').removeClass('open');
});